package com.scipionyx.butterflyeffect.model.ui.view;

import com.scipionyx.butterflyeffect.frontend.ui.view.common.AbstractView;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * 
 * 
 * 
 * @author Renato Mendes
 *
 */
abstract class AbstractDataModelView extends AbstractView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected boolean editing;

	/**
	 * 
	 */
	@Override
	public void doBuildLeftMenu(VerticalLayout leftMenuPanel) {

		// Data Sources
		Button buttonDataSources = new Button("");
		buttonDataSources.setIcon(FontAwesome.DATABASE);
		buttonDataSources.setStyleName(ValoTheme.BUTTON_BORDERLESS);

		buttonDataSources.addClickListener(new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				userMenuService.getNavigator().navigateTo("dataSourceConfigurationView");
			}
		});

		Button buttonDataModel = new Button("");
		buttonDataModel.setIcon(FontAwesome.UNIVERSITY);
		buttonDataModel.setStyleName(ValoTheme.BUTTON_BORDERLESS);
		buttonDataModel.addClickListener(new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				userMenuService.getNavigator().navigateTo("dataModelConfigurationView");

			}
		});

		leftMenuPanel.addComponent(buttonDataSources);
		leftMenuPanel.setComponentAlignment(buttonDataSources, Alignment.TOP_CENTER);
		leftMenuPanel.addComponent(buttonDataModel);
		leftMenuPanel.setComponentAlignment(buttonDataModel, Alignment.TOP_CENTER);
		leftMenuPanel.setExpandRatio(buttonDataModel, 1);

	}

}
