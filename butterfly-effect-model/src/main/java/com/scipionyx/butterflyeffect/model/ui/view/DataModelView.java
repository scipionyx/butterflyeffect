package com.scipionyx.butterflyeffect.model.ui.view;

import org.vaadin.dialogs.ConfirmDialog;

import com.scipionyx.butterflyeffect.frontend.model.Title;
import com.scipionyx.butterflyeffect.model.model.datamodel.Entity;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * 
 * 
 * @version 0.1.0
 * @author Renato Mendes
 *
 */
@SpringComponent("dataModelConfigurationView")
@UIScope()
public class DataModelView extends AbstractDataModelView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean editing;

	private VerticalLayout workAreaPanel;

	private TabSheet sheet;

	/**
	 * 
	 */
	@Override
	public void doBuild() {
	}

	@Override
	public void doBuildWorkArea(VerticalLayout workAreaPanel, Title tile) {
		this.workAreaPanel = workAreaPanel;

		// Setting up the title
		tile.setTitle("Data Model");
		tile.setSubTitle("Data Model Configuration Tool");

		sheet = new TabSheet();

		// Entities
		VerticalLayout entitiesPanel = new VerticalLayout();
		doBuildTab(entitiesPanel);

		// Events
		VerticalLayout eventsPanel = new VerticalLayout();
		doBuildTab(eventsPanel);

		// Facts
		VerticalLayout factsPanel = new VerticalLayout();
		doBuildTab(factsPanel);

		// Incidents
		VerticalLayout incidentsPanel = new VerticalLayout();
		doBuildTab(incidentsPanel);

		// Reference Tables
		VerticalLayout referenceTablesPanel = new VerticalLayout();
		doBuildTab(referenceTablesPanel);

		sheet.addTab(entitiesPanel, "Entities");
		sheet.addTab(eventsPanel, "Events");
		sheet.addTab(factsPanel, "Facts");
		sheet.addTab(incidentsPanel, "Incidents");
		sheet.addTab(referenceTablesPanel, "Reference Tables");

		workAreaPanel.addComponent(sheet);

	}

	/**
	 * 
	 * @param layout
	 */
	private void doBuildTab(VerticalLayout layout) {

		layout.setMargin(new MarginInfo(true, false, false, false));

		Grid grid = new Grid();

		layout.addComponent(grid);

	}

	/**
	 * 
	 */

	public void doLeftPanelBuild(VerticalLayout leftMenuPanel) {

		//

		Accordion accordion = new Accordion();

		accordion.setSizeFull();

		// Entities Tab
		Layout entitiesTab = new VerticalLayout();
		entitiesTab.addComponent(new Label("entitiesTab"));
		entitiesTab.addComponent(new Label("entitiesTab"));
		entitiesTab.addComponent(new Label("entitiesTab"));
		entitiesTab.addComponent(new Label("entitiesTab"));
		accordion.addTab(entitiesTab, "Entities (3)");

		// Events Tab
		Layout eventsTab = new VerticalLayout();
		eventsTab.addComponent(new Label("Events"));
		accordion.addTab(eventsTab, "Events (1)");

		// Facts Tab
		Layout factsTab = new VerticalLayout();
		factsTab.addComponent(new Label("Events"));
		accordion.addTab(factsTab, "Facts");

		// Incidents Tab
		Layout incidentsTab = new VerticalLayout();
		incidentsTab.addComponent(new Label("incidentsTab"));
		accordion.addTab(incidentsTab, "Incidents");

		// Alerts Tab
		Layout alertsTab = new VerticalLayout();
		alertsTab.addComponent(new Label("alertsTab"));
		accordion.addTab(alertsTab, "Alerts");

		leftMenuPanel.addComponent(accordion);

	}

	/**
	 * 
	 * @return
	 */
	private void createWorkingAreaPanel(Entity entity, Layout target) {

		if (entity == null) {
			entity = new Entity();
			entity.setName("");
			entity.setDescription("");
			entity.setLabel("");
		}

		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		target.addComponent(horizontalLayout);

		createEntityForm(entity);

		FormLayout formLayout = new FormLayout();
		formLayout.setCaption("Edit Entity Information");

		BeanFieldGroup<Entity> binder = new BeanFieldGroup<>(Entity.class);
		binder.setItemDataSource(entity);

		// Name
		formLayout.addComponent(binder.buildAndBind("Name", "name"));

		// Description
		// layout.addComponent(binder.buildAndBind("Description",
		// "description"));

		// Description
		formLayout.addComponent(binder.buildAndBind("Description", "description", TextArea.class));

		// Label
		formLayout.addComponent(binder.buildAndBind("Label", "label"));

		// Class

		// Type
		formLayout.addComponent(binder.buildAndBind("Type", "type", ComboBox.class));

		// Buffer the form content
		binder.setBuffered(true);

		//
		formLayout.addComponent(new Button("OK", new ClickListener() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				try {
					binder.commit();
					Notification.show("Entity Saved", Notification.Type.HUMANIZED_MESSAGE);
					target.removeAllComponents();

				} catch (CommitException e) {
				}
			}
		}));

		horizontalLayout.addComponent(formLayout);

		//////

		VerticalLayout verticalLayout = new VerticalLayout();
		horizontalLayout.addComponent(verticalLayout);

		Button addFieldButton = new Button("Add Field");

		Grid grid = new Grid();
		grid.setWidthUndefined();
		grid.setCaption("Fields");
		grid.setColumnReorderingAllowed(true);
		grid.setHeightByRows(5);

		grid.setEditorEnabled(true);
		grid.setSelectionMode(SelectionMode.NONE);

		grid.addColumn("Name", String.class).setRenderer(new TextRenderer()).setExpandRatio(2);
		grid.addColumn("Description", String.class).setRenderer(new TextRenderer()).setExpandRatio(2);
		grid.addColumn("Type", String.class).setRenderer(new TextRenderer()).setExpandRatio(2);
		grid.addColumn("Size", Integer.class).setRenderer(new NumberRenderer()).setExpandRatio(2);

		addFieldButton.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				grid.addRow("<field name>", "<field description>", "<field type>", 0);
			}
		});

		verticalLayout.addComponent(grid);
		verticalLayout.addComponent(addFieldButton);

	}

	private void createEntityForm(Entity entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doBuildBottomArea(HorizontalLayout buttomAreaPanel) {
		// Add new button
		Button button = new Button("Add new");
		button.setIcon(FontAwesome.PLUS);
		button.setStyleName(ValoTheme.BUTTON_BORDERLESS);
		buttomAreaPanel.addComponent(button);
		buttomAreaPanel.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);
		// buttomAreaPanel.setExpandRatio(button, 1);

		button.addClickListener(new Button.ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {

				if (editing) {
					ConfirmDialog.show(event.getComponent().getUI(),
							"This will cancel the edition of the Entity, continue ?", new ConfirmDialog.Listener() {

						private static final long serialVersionUID = 1L;

						@Override
						public void onClose(ConfirmDialog dialog) {

							if (dialog.isConfirmed()) {
								workAreaPanel.removeAllComponents();
								createWorkingAreaPanel(null, workAreaPanel);
								editing = true;
							}

						}
					});
				} else {

					workAreaPanel.removeAllComponents();
					createWorkingAreaPanel(null, workAreaPanel);
					editing = true;

				}

			}
		});

	}

}
