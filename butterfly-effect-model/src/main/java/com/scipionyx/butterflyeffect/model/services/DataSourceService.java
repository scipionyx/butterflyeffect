package com.scipionyx.butterflyeffect.model.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.scipionyx.butterflyeffect.infrastructure.common.IConfigurationHandler;
import com.scipionyx.butterflyeffect.model.model.datasource.ORMDataSource;

/**
 * 
 * 
 * 
 * 
 * @author Renato Mendes
 *
 */
@Service()
public class DataSourceService implements IConfigurationHandler<ORMDataSource> {

	@Override
	public void write(ORMDataSource t) {

	}

	@Override
	public List<ORMDataSource> readAll() {
		return null;
	}

	@Override
	public void verify(ORMDataSource t) {

	}

}
