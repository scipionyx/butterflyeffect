package com.scipionyx.be.etlengine;

import java.net.MalformedURLException;

import javax.sql.DataSource;

import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.job.builder.JobFlowBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.builder.SimpleStepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.scipionyx.be.etlengine.listener.DefaultItemReadListener;
import com.scipionyx.be.etlengine.listener.JobCompletionNotificationListener;
import com.scipionyx.be.etlengine.reader.ItemReaderFactory;
import com.scipionyx.be.etlengine.reader.ReaderType;
import com.scipionyx.be.etlengine.reader.ResourceFactory;
import com.scipionyx.be.etlengine.writer.DummyItemWriter;

/**
 * 
 * @author Renato Mendes
 *
 */
// @Configuration
// @EnableBatchProcessing
public class JobConfigurationDDD<T> {

	@Autowired
	private ResourceFactory resourceFactory;

	@Autowired
	private ItemReaderFactory<T> itemReaderFactory;

	/**
	 * Creates the item reader.
	 * 
	 * TODO: obtain the resource directly from the web <br>
	 * TODO: create the marshaler
	 * 
	 * @return
	 * @throws MalformedURLException
	 */
	@Bean
	public ItemReader<T> reader() throws MalformedURLException {
		return itemReaderFactory.createReader(ReaderType.CSV);
	}

	/**
	 * 
	 * @return
	 */
	@Bean
	public ItemReadListener<?> itemReadListener() {
		return new DefaultItemReadListener();
	}

	/**
	 * 
	 * @return
	 */
	@Bean
	public JobCompletionNotificationListener jobCompletionNotificationListener() {
		return new JobCompletionNotificationListener();
	}

	/**
	 * Creates the item Processor TODO - use Factory for providing the Processor
	 * 
	 * @return
	 */
	@Bean
	public ItemProcessor<Object, Object> processor() {
		return null; // new SdnItemProcessor();
	}

	/**
	 * Create Item Writer TODO - Use Factory to return the writer
	 * 
	 * @param dataSource
	 * @return
	 */
	@Bean
	public ItemWriter<Object> writer(DataSource dataSource) {
		return new DummyItemWriter<Object>();
	}

	/**
	 * 
	 * @param jobs
	 * @param step01
	 * @param listener
	 * @return
	 */
	@Bean
	public Job downloadAndIndexSDN(JobBuilderFactory jobs, Step step01, JobExecutionListener listener) {

		JobBuilder builder = jobs.get("DownloadAndIndexSDN");

		// Define the incrementer
		builder.incrementer(new RunIdIncrementer());

		// Add listener
		builder.listener(listener);

		// Define the flow
		JobFlowBuilder flowBuilder = builder.flow(step01);
		flowBuilder.end();

		// Create Job
		return flowBuilder.build().build();

	}

	/// Defining steps

	/**
	 * This is the step on the job execution. Here the data will be downloaded
	 * from the server and the system will parser the data into documents, then
	 * the documents will be sent to the ElasticSearch
	 * 
	 * @param stepBuilderFactory
	 * @param reader
	 * @param writer
	 * @param processor
	 * @return
	 */
	@Bean(name = "parseData")
	public Step stepParseData(StepBuilderFactory stepBuilderFactory, ItemReader<T> reader, ItemWriter<T> writer,
			ItemProcessor<T, T> processor, ItemReadListener<T> itemReadListener) {

		// TODO Set the name of the step
		// TODO set the size of the chunk
		SimpleStepBuilder<T, T> stepBuilder = stepBuilderFactory.get("parseData").<T, T> chunk(10);

		// Add reader
		// Readers are obligatory
		stepBuilder.reader(reader);

		// Add processor
		// Processor(s) are optional
		if (processor != null) {
			stepBuilder.processor(processor);
		}

		// Add writer
		// Writers are Obligatory
		stepBuilder.writer(writer);

		// Add Listeners
		// LIstners are part of the framework
		stepBuilder.listener(itemReadListener);

		// Build the step
		return stepBuilder.build();

	}

	public ResourceFactory getResourceFactory() {
		return resourceFactory;
	}

	public void setResourceFactory(ResourceFactory resourceFactory) {
		this.resourceFactory = resourceFactory;
	}

	public ItemReaderFactory<T> getItemReaderFactory() {
		return itemReaderFactory;
	}

	public void setItemReaderFactory(ItemReaderFactory<T> itemReaderFactory) {
		this.itemReaderFactory = itemReaderFactory;
	}

}
