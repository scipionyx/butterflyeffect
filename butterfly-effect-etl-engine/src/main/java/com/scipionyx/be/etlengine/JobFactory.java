package com.scipionyx.be.etlengine;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Renato Mendes
 *
 */
@Configuration
public class JobFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(JobFactory.class);

	/**
	 * 
	 * @return
	 */
	public List<JobBean> readJobConfiguration() {
		LOGGER.debug("Creating Jobs");
		return null;
	}

}
