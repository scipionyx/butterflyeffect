package com.scipionyx.be.etlengine.listener;

import org.springframework.batch.core.ItemReadListener;

/**
 * 
 * @author Renato Mendes
 *
 */
public class DefaultItemReadListener implements ItemReadListener<Object> {

	private int expectedCount;

	private int errorCount;

	private int readCount;

	/**
	 * 
	 */
	@Override
	public void beforeRead() {
	}

	/**
	 * 
	 */
	@Override
	public void afterRead(Object item) {
	}

	/**
	 * 
	 */
	@Override
	public void onReadError(Exception ex) {
	}

	public int getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(int errorCount) {
		this.errorCount = errorCount;
	}

	public int getReadCount() {
		return readCount;
	}

	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}

	public int getExpectedCount() {
		return expectedCount;
	}

	public void setExpectedCount(int expectedCount) {
		this.expectedCount = expectedCount;
	}

}
