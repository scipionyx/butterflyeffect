package com.scipionyx.be.etlengine.configuration;

import java.io.Serializable;

/**
 * 
 * Describes a New Job Configuration
 * 
 * @author rmendes
 *
 */
public class JobConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private ReaderConfiguration readerConfiguration;

	private ProcessorConfiguration processorConfiguration;

	private WriterConfiguration writerConfiguration;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ReaderConfiguration getReaderConfiguration() {
		return readerConfiguration;
	}

	public void setReaderConfiguration(ReaderConfiguration readerConfiguration) {
		this.readerConfiguration = readerConfiguration;
	}

	public ProcessorConfiguration getProcessorConfiguration() {
		return processorConfiguration;
	}

	public void setProcessorConfiguration(ProcessorConfiguration processorConfiguration) {
		this.processorConfiguration = processorConfiguration;
	}

	public WriterConfiguration getWriterConfiguration() {
		return writerConfiguration;
	}

	public void setWriterConfiguration(WriterConfiguration writerConfiguration) {
		this.writerConfiguration = writerConfiguration;
	}

}
