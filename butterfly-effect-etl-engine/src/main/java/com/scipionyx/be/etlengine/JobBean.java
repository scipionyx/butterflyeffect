package com.scipionyx.be.etlengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;

/**
 * 
 * @author Renato Mendes
 *
 */
@EnableBatchProcessing
public class JobBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(JobBean.class);

}
