package com.scipionyx.be.etlengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 * @author Renato Mendes
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = "{com.scipionyx.be.etlengine.reader,com.scipionyx.be.etlengine}")
public class Application {

	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

}
