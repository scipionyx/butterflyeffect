package com.scipionyx.be.etlengine.configuration;

import com.scipionyx.be.etlengine.reader.ReaderType;

/**
 * 
 * @author Renato Mendes
 *
 */
public class ReaderConfiguration {

	private ReaderType type;

	private String resource;

	public ReaderType getType() {
		return type;
	}

	public void setType(ReaderType type) {
		this.type = type;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

}
