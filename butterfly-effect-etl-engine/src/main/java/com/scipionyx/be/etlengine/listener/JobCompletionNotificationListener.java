package com.scipionyx.be.etlengine.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Renato Mendes
 *
 */
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

	@Autowired
	private DefaultItemReadListener itemReadListener;

	/**
	 * 
	 */
	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == org.springframework.batch.core.BatchStatus.COMPLETED) {
			//
			log.info("JOB FINISHED, read count: {}/{}", itemReadListener.getReadCount(),
					itemReadListener.getExpectedCount());

		}
	}

	public DefaultItemReadListener getItemReadListener() {
		return itemReadListener;
	}

	public void setItemReadListener(DefaultItemReadListener itemReadListener) {
		this.itemReadListener = itemReadListener;
	}

}
