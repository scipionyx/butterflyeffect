package com.scipionyx.be.etlengine.reader;

import java.net.MalformedURLException;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Renato Mendes
 *
 */
@Component
public class ResourceFactory {

	/**
	 * 
	 * @return
	 * @throws MalformedURLException
	 */
	public Resource createResource(ResourceType resourceType, String resource) throws MalformedURLException {

		switch (resourceType) {
		case FileSystem:
			return new FileSystemResource(resource);

		case URL:
		default:
			return new UrlResource(resource);
		}

	}

}
