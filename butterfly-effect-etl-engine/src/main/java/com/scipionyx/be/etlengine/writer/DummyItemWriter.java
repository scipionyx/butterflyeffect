package com.scipionyx.be.etlengine.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

/**
 * 
 * @author Renato Mendes
 *
 * @param <T>
 */
public class DummyItemWriter<T> implements ItemWriter<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DummyItemWriter.class);

	private int total;

	/**
	 * 
	 */
	@Override
	public void write(List<? extends T> items) throws Exception {

		for (T t : items) {
			LOGGER.debug("The following class {} was detected.", t.getClass().getName());
		}

	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
