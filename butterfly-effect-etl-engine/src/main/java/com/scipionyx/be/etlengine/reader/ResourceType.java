package com.scipionyx.be.etlengine.reader;

/**
 * 
 * @author Renato Mendes
 *
 */
public enum ResourceType {

	URL, FileSystem

}
