package com.scipionyx.be.etlengine.reader;

import java.net.MalformedURLException;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Renato Mendes
 *
 */
@Component
public class ItemReaderFactory<T> {

	@Autowired
	private ResourceFactory resourceFactory;

	/**
	 * 
	 * @return
	 * @throws MalformedURLException
	 */
	public final ItemReader<T> createReader(ReaderType readerType) throws MalformedURLException {

		switch (readerType) {
		case CSV:
			return createCSVReader(null, null);

		case Custom:
		default:
			return null;
		}

	}

	/**
	 * 
	 * @param resourceType
	 * @param resourceName
	 * @return
	 * @throws MalformedURLException
	 */
	private ItemReader<T> createCSVReader(ResourceType resourceType, String resourceName) throws MalformedURLException {

		FlatFileItemReader<T> fileItemReader = new FlatFileItemReader<>();

		Resource resource = resourceFactory.createResource(resourceType, resourceName);

		fileItemReader.setResource(resource);

		return fileItemReader;
	}

	public ResourceFactory getResourceFactory() {
		return resourceFactory;
	}

	public void setResourceFactory(ResourceFactory resourceFactory) {
		this.resourceFactory = resourceFactory;
	}

}
