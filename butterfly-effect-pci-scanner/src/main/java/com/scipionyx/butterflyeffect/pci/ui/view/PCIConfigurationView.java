package com.scipionyx.butterflyeffect.pci.ui.view;

import com.scipionyx.butterflyeffect.frontend.model.Title;
import com.scipionyx.butterflyeffect.frontend.ui.view.common.AbstractView;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

/**
 * 
 * 
 * 
 * @version 0.1.0
 * @author Renato Mendes
 *
 */
@SpringComponent("PCIConfigurationView")
@UIScope()
public class PCIConfigurationView extends AbstractView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@Override
	public void enter(ViewChangeEvent event) {

	}

	/**
	 * 
	 */
	@Override
	public void doBuild() {
		// getTitlePanel().setTitle("Welcome - Plugin View - doBuild");
	}

	@Override
	public void build() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doBuildLeftMenu(VerticalLayout leftMenuPanel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doBuildWorkArea(VerticalLayout workAreaPanel, Title tile) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doBuildBottomArea(HorizontalLayout buttomAreaPanel) {
		// TODO Auto-generated method stub

	}

}
