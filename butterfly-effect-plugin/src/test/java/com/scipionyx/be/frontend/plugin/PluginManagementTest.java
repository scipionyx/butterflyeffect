package com.scipionyx.be.frontend.plugin;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.scipionyx.butterflyeffect.plugin.model.Plugin;
import com.scipionyx.butterflyeffect.plugin.services.ManagementService;

/**
 * 
 * @author Renato Mendes
 *
 */
public class PluginManagementTest {

	/**
	 * @throws IOException
	 * 
	 */
	@Test
	public void testReadPlugins() throws IOException {

		ManagementService management = new ManagementService();
		management.init();

		management.readConfigurations();

		System.out.println(management.getConfigurations().size());

	}

	/**
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 * 
	 */
	@Test
	public void testWritePlugin() throws JsonGenerationException, JsonMappingException, IOException {

		Plugin plugin = new Plugin();
		plugin.setName("Renato");
		plugin.setRelease(plugin.new Release());
		plugin.getRelease().setDate("10/10/10");
		plugin.getRelease().setVersion("01.01.01");
		plugin.setVendor(plugin.new Vendor());
		plugin.getVendor().setName("Scipionyx Software");
		plugin.getVendor().setUrl("www.ScipionyxSoft.com");

		ManagementService management = new ManagementService();
		management.init();

		OutputStream outputStream = new FileOutputStream("/Users/rmendes/Desktop/test.info");

		management.writePlugin(outputStream, plugin);

	}

}
