package com.scipionyx.butterflyeffect.infrastructure.common;

import java.util.List;

/**
 * 
 * @author Renato Mendes
 *
 * @param <T>
 */
public interface IConfigurationHandler<T> {

	public void verify(T t);

	public void write(T t);

	public List<T> readAll();

}
