package com.scipionyx.butterflyeffect.frontend.ui.view.common;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

/**
 * 
 * @author Renato Mendes
 *
 */
public class NavigationCommand implements Command {

	private Navigator navigator;

	private String targetView;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param rootView
	 * @param targetView
	 */
	public NavigationCommand(Navigator navigator, String targetView) {
		super();
		this.targetView = targetView;
		this.navigator = navigator;
	}

	/**
	 * 
	 */
	@Override
	public void menuSelected(MenuItem selectedItem) {
		navigator.navigateTo(targetView);
	}

}
