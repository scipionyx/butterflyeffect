package com.scipionyx.butterflyeffect.frontend.ui.panel.workarea;

import com.scipionyx.butterflyeffect.frontend.ui.panel.common.AbstractPanel;

/**
 * 
 * @author Renato Mendes
 *
 */
public abstract class AbstractWorkAreaPanel extends AbstractPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
