package com.scipionyx.butterflyeffect.frontend.ui.view.common;

import org.springframework.beans.factory.annotation.Autowired;

import com.scipionyx.butterflyeffect.frontend.model.Title;
import com.scipionyx.butterflyeffect.frontend.services.UserMenuService;
import com.scipionyx.butterflyeffect.frontend.ui.panel.top.TopPanel;
import com.scipionyx.butterflyeffect.frontend.ui.panel.workarea.WorkAreaPanel;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

/**
 * 
 * 
 * 
 * 
 * @author Renato Mendes
 *
 */
public abstract class AbstractView extends VerticalLayout implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Services
	@Autowired
	protected UserMenuService userMenuService;

	// Main Panels
	protected TopPanel topPanel;

	//
	protected WorkAreaPanel workAreaPanel;

	//
	protected boolean built;

	public abstract void doBuildLeftMenu(VerticalLayout leftMenuPanel);

	public abstract void doBuildWorkArea(VerticalLayout workAreaPanel, Title tile);

	public abstract void doBuildBottomArea(HorizontalLayout buttomAreaPanel);

	public abstract void doBuild();

	/**
	 * 
	 * @throws Exception
	 */
	public void build() throws Exception {

		if (built) {
			return;
		}

		built = true;

		//
		//
		this.setMargin(new MarginInfo(false, true, false, true));
		this.setSizeFull();
		this.setSpacing(false);

		// Top panel
		//
		topPanel = new TopPanel(userMenuService);
		topPanel.build();
		this.addComponent(topPanel);

		// Main work Area
		//
		GridLayout backLayout = new GridLayout(2, 1);
		backLayout.setSizeFull();
		backLayout.setColumnExpandRatio(0, 1);
		backLayout.setColumnExpandRatio(1, 20);

		this.addComponent(backLayout);
		this.setExpandRatio(backLayout, 1);

		// Left Menu
		//
		VerticalLayout left = new VerticalLayout();
		left.addStyleName(ButterflyEffectTheme.MENU_LEFT);
		left.setSizeFull();
		doBuildLeftMenu(left);
		backLayout.addComponent(left);

		// Main work Area
		//
		workAreaPanel = new WorkAreaPanel();
		workAreaPanel.build();
		backLayout.addComponent(workAreaPanel);

		//
		//
		Title title = new Title();
		doBuildWorkArea(workAreaPanel.getWorkPanel(), title);
		workAreaPanel.setTitle(title.getTitle());
		workAreaPanel.setSubTitle(title.getSubTitle());
		doBuildBottomArea(workAreaPanel.getBottomPanel());

		//
		doBuild();

		//

	}

	public UserMenuService getUserMenuService() {
		return userMenuService;
	}

	public void setUserMenuService(UserMenuService userMenuService) {
		this.userMenuService = userMenuService;
	}

}
