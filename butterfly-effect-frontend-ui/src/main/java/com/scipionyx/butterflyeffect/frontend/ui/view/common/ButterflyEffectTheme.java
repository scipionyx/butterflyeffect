package com.scipionyx.butterflyeffect.frontend.ui.view.common;

import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * @author Renato Mendes
 *
 */
public class ButterflyEffectTheme extends ValoTheme {

	/**
	 * Styles the notification to look like {@link Type#TRAY_NOTIFICATION},
	 * without setting the position and delay. Can be combined with any other
	 * Notification style.
	 */
	public static final String MENU_LEFT = "menuleft";

}
