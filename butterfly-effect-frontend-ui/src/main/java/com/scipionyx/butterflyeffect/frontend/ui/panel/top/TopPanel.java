package com.scipionyx.butterflyeffect.frontend.ui.panel.top;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.scipionyx.butterflyeffect.frontend.model.Menu;
import com.scipionyx.butterflyeffect.frontend.services.UserMenuService;
import com.scipionyx.butterflyeffect.frontend.ui.panel.common.AbstractPanel;
import com.scipionyx.butterflyeffect.frontend.ui.view.common.NavigationCommand;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * 
 * 
 * 
 * @author Renato Mendes
 *
 */
public class TopPanel extends AbstractPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserMenuService userMenuService;

	/**
	 * 
	 * @param menuService
	 */
	public TopPanel(UserMenuService userMenuService) {
		this.userMenuService = userMenuService;
	}

	/**
	 * @throws IOException
	 * 
	 */
	@Override
	public void build() {

		this.setSizeFull();
		this.setSpacing(false);
		this.setHeight(40, Unit.PIXELS);
		this.setMargin(new MarginInfo(false, false, false, false));

		//
		GridLayout gridLayout = new GridLayout(3, 1);
		gridLayout.setPrimaryStyleName(ValoTheme.MENU_ROOT);
		gridLayout.setSizeFull();
		gridLayout.setSpacing(false);
		gridLayout.setColumnExpandRatio(0, 3);
		gridLayout.setColumnExpandRatio(1, 10);
		gridLayout.setColumnExpandRatio(2, 1);

		// Label
		Button logo = new Button();
		logo.setIcon(FontAwesome.BITBUCKET);
		logo.setCaptionAsHtml(true);
		logo.setCaption("Butterfly <b>Effect</b>");
		logo.setSizeFull();
		logo.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		gridLayout.addComponent(logo, 0, 0);
		gridLayout.setComponentAlignment(logo, Alignment.MIDDLE_LEFT);

		// Menu
		MenuBar menuBar = new MenuBar();
		menuBar.setStyleName(ValoTheme.MENUBAR_BORDERLESS);
		buildMainMenuBar(userMenuService, menuBar);
		gridLayout.addComponent(menuBar, 1, 0);
		gridLayout.setComponentAlignment(menuBar, Alignment.MIDDLE_LEFT);

		// User Menu
		Button userMenu = new Button();
		userMenu.setSizeFull();
		userMenu.setStyleName(ValoTheme.BUTTON_BORDERLESS);
		userMenu.setIcon(FontAwesome.GEAR);
		gridLayout.addComponent(userMenu, 2, 0);
		gridLayout.setComponentAlignment(userMenu, Alignment.MIDDLE_RIGHT);

		// Finally add the whole thing to the root view
		this.addComponent(gridLayout);
	}

	/**
	 * @throws IOException
	 * 
	 */
	private void buildMainMenuBar(UserMenuService userMenuService, MenuBar menuBar) {
		//

		Map<String, MenuItem> menus = new HashMap<>();

		for (Menu menu : userMenuService.getMenuService().getConfigurations()) {

			String id = (menu.getId() != null) ? menu.getId() : "NO-LABEL";
			String label = (menu.getLabel() != null) ? menu.getLabel() : "NO-LABEL";

			NavigationCommand command = (menu.getView() != null)
					? new NavigationCommand(userMenuService.getNavigator(), menu.getView()) : null;

			if (menu.isVisible()) {
				if (menu.getParent() == null) {
					MenuItem menuItem = menuBar.addItem(label, command);
					menus.put(id, menuItem);
				} else {
					MenuItem parentMenuItem = menus.get(menu.getParent());
					if (parentMenuItem != null) {
						MenuItem menuItem = parentMenuItem.addItem(label, command);
						menus.put(id, menuItem);
					}
				}
			}

		}

	}

}
