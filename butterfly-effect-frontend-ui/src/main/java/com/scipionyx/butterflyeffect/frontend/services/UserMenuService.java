package com.scipionyx.butterflyeffect.frontend.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.scipionyx.butterflyeffect.frontend.model.Menu;
import com.scipionyx.butterflyeffect.frontend.ui.view.common.AbstractView;
import com.scipionyx.butterflyeffect.frontend.ui.view.root.RootView;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

/**
 * 
 * This service is responsible for creating and rendering the navigation
 * depending on the user permissions
 * 
 * TODO - obtain logged user <br>
 * TODO - identify menus that should not be rendered<br>
 * 
 * 
 * @author Renato Mendes
 *
 */
@SpringComponent("userMenuService")
@UIScope()
public class UserMenuService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private MenuService menuService;

	private Navigator navigator;

	/**
	 * @param rootView
	 * @throws Exception
	 * 
	 */
	public void build(Navigator navigator, RootView rootView) throws Exception {

		this.navigator = navigator;

		// Build Views
		createNavigationBuildViews(rootView);

	}

	/**
	 * TODO - Handle parent that does no exist<br>
	 * TODO - Handle view that does not exist as a bean
	 * 
	 * @param navigator
	 * @throws Exception
	 */
	private void createNavigationBuildViews(RootView rootView) throws Exception {

		// Add root view to the Navigator.
		navigator.addView("", rootView);
		rootView.build();

		for (Menu menu : menuService.getConfigurations()) {
			String viewName = menu.getView();
			if (viewName != null) {
				Object viewObject = applicationContext.getBean(viewName);
				if (viewObject != null) {
					navigator.addView(viewName, (View) viewObject);
					((AbstractView) viewObject).build();
				}
			}
		}

	}

	/**
	 * 
	 * @return
	 */
	public Navigator getNavigator() {
		return navigator;
	}

	public MenuService getMenuService() {
		return menuService;
	}

}
