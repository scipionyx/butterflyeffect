package com.scipionyx.butterflyeffect.frontend.ui.panel.workarea;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * 
 * 
 * @author Renato Mendes
 *
 */
public class LeftPanel extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private VerticalLayout internal;

	private final static float SIZE_EXPANDED = 250;
	private final static float SIZE_COLAPSED = 70;

	/**
	 * 
	 */
	public void build() {

		//
		this.setHeight(100, Unit.PERCENTAGE);
		this.setSpacing(true);
		this.setMargin(false);
		this.setWidth(SIZE_COLAPSED, Unit.PIXELS);
		this.addStyleName(ValoTheme.MENU_ROOT);

		//
		internal = new VerticalLayout();
		internal.setSizeFull();
		this.addComponent(internal);

		//
		Button expandButton = new Button();
		expandButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		expandButton.setIcon(FontAwesome.ARROW_RIGHT);
		this.addComponent(expandButton);
		this.setComponentAlignment(expandButton, Alignment.BOTTOM_RIGHT);

		expandButton.addClickListener(new Button.ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (event.getComponent().getIcon() == FontAwesome.ARROW_RIGHT) {
					event.getComponent().getParent().setWidth(SIZE_EXPANDED, Unit.PIXELS);
					event.getComponent().setIcon(FontAwesome.ARROW_LEFT);
				} else {
					event.getComponent().getParent().setWidth(SIZE_COLAPSED, Unit.PIXELS);
					event.getComponent().setIcon(FontAwesome.ARROW_RIGHT);
				}
			}
		});

	}

	/**
	 * 
	 * @return
	 */
	public VerticalLayout getInternal() {
		return internal;
	}

}
