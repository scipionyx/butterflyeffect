package com.scipionyx.butterflyeffect.frontend.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.scipionyx.butterflyeffect.frontend.model.Menu;
import com.scipionyx.butterflyeffect.frontend.model.MenuComparator;

/**
 * 
 * This service is responsible for reading the default configuration of the
 * menus from the plugins
 * 
 * 
 * @author Renato Mendes
 *
 */
@Service("MenuService")
public class MenuService extends AbstractConfigurationService<Menu> {

	/**
	 * @throws IOException
	 * 
	 */
	@PostConstruct
	public void init() throws IOException {
		super.init();
	}

	/**
	 * This function will read the main menu configuration for all services.
	 * 
	 * @throws IOException
	 */
	@Override
	public void readConfigurations() throws IOException {
		//
		List<InputStream> configurations = loadResources("menu.info", null);
		//
		for (InputStream inputStream : configurations) {
			Menu[] _menus = getObjectMapper().readValue(inputStream, Menu[].class);
			Collections.addAll(getConfigurations(), _menus);
		}

		// Sort Menu
		Collections.sort(getConfigurations(), new MenuComparator());

	}

}
