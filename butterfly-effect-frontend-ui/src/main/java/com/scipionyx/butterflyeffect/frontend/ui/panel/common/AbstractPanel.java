package com.scipionyx.butterflyeffect.frontend.ui.panel.common;

import com.vaadin.ui.VerticalLayout;

/**
 * 
 * @author Renato Mendes
 *
 */
public abstract class AbstractPanel extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract void build() throws Exception;

}
