package com.scipionyx.butterflyeffect.frontend.ui.panel.workarea;

import java.io.IOException;

import org.springframework.util.StringUtils;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * 
 * @author Renato Mendes
 *
 */
public class WorkAreaPanel extends AbstractWorkAreaPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1;

	private VerticalLayout workPanel;

	private Label titleLabel;

	private Label subTitleLabel;

	private HorizontalLayout buttomPanel;

	/**
	 * @throws IOException
	 * 
	 */
	public final void build() throws IOException {

		//
		this.setSizeFull();

		// Main layout for the work area
		GridLayout gridLayout = new GridLayout(2, 2);
		gridLayout.setSizeFull();
		gridLayout.setSpacing(false);
		gridLayout.setMargin(new MarginInfo(false, false, false, false));
		gridLayout.setColumnExpandRatio(0, 1);
		gridLayout.setColumnExpandRatio(1, 1);
		gridLayout.setRowExpandRatio(0, 1);
		gridLayout.setRowExpandRatio(1, 100);

		// Add Title
		gridLayout.addComponent(buildTitleLayout(), 0, 0);

		// Add Button
		gridLayout.addComponent(buildButtonLayout(), 1, 0);

		// Add WorkArea
		gridLayout.addComponent(buildWorkAreaLayout(), 0, 1, 1, 1);

		//
		this.addComponent(gridLayout);

	}

	/**
	 * 
	 * @return
	 */
	private Component buildWorkAreaLayout() {
		workPanel = new VerticalLayout();
		workPanel.setSizeFull();
		return workPanel;
	}

	/**
	 * 
	 * @return
	 */
	private Component buildButtonLayout() {
		buttomPanel = new HorizontalLayout();
		buttomPanel.setSizeFull();
		return buttomPanel;
	}

	/**
	 * 
	 */
	private AbstractLayout buildTitleLayout() {

		HorizontalLayout titleLayout = new HorizontalLayout();
		titleLayout.setSpacing(true);

		titleLabel = new Label();
		titleLabel.addStyleName(ValoTheme.LABEL_H1);
		titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		titleLayout.addComponent(titleLabel);
		titleLayout.setComponentAlignment(titleLabel, Alignment.BOTTOM_LEFT);

		subTitleLabel = new Label();
		subTitleLabel.addStyleName(ValoTheme.LABEL_H2);
		// subTitleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		titleLayout.addComponent(subTitleLabel);
		titleLayout.setComponentAlignment(subTitleLabel, Alignment.BOTTOM_CENTER);

		return titleLayout;

	}

	public VerticalLayout getWorkPanel() {
		return workPanel;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		if (!StringUtils.isEmpty(title)) {
			titleLabel.setValue(title);
		} else {
			// titleGridLayout.removeComponent(this.titleLabel);
		}
	}

	/**
	 * 
	 * @param subTitle
	 */
	public void setSubTitle(String subTitle) {
		if (!StringUtils.isEmpty(subTitle)) {
			subTitleLabel.setValue(subTitle);
		} else {
			// titleGridLayout.removeComponent(this.subTitleLabel);
		}

	}

	public HorizontalLayout getBottomPanel() {
		return buttomPanel;
	}

}
