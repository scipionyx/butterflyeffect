package com.scipionyx.butterflyeffect.frontend.ui.panel.top;

import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

/**
 * 
 * @author Renato Mendes
 *
 */
public class UserMenuPanel extends HorizontalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private AbstractOrderedLayout topPanel;

	/**
	 * 
	 */
	public void build() {
		this.addComponent(new Label("UserMenu"));
		topPanel.addComponent(this);
		topPanel.setComponentAlignment(this, Alignment.MIDDLE_CENTER);
	}

	public AbstractOrderedLayout getTopPanel() {
		return topPanel;
	}

	public void setTopPanel(AbstractOrderedLayout topPanel) {
		this.topPanel = topPanel;
	}

}
