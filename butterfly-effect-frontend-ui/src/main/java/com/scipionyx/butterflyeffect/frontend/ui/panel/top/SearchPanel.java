package com.scipionyx.butterflyeffect.frontend.ui.panel.top;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

/**
 * 
 * @author Renato Mendes
 *
 */
public class SearchPanel extends HorizontalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3170142895796718380L;

	private TextField searchTextField;

	private AbstractOrderedLayout topPanel;

	private Button executeButton;

	/**
	 * 
	 */
	public void build() {

		this.setStyleName("SearchPanel");

		//
		searchTextField = new TextField();
		searchTextField.setValue("Search");
		this.addComponent(searchTextField);

		// Execute Search
		executeButton = new Button();
		executeButton.setIcon(new ThemeResource("icons/Blue/Search.png"));
		this.addComponent(executeButton);

		//
		topPanel.addComponent(this);
		topPanel.setComponentAlignment(this, Alignment.MIDDLE_RIGHT);

	}

	public TextField getSearch() {
		return searchTextField;
	}

	public void setSearch(TextField search) {
		this.searchTextField = search;
	}

	public AbstractOrderedLayout getTopPanel() {
		return topPanel;
	}

	public void setTopPanel(AbstractOrderedLayout topPanel) {
		this.topPanel = topPanel;
	}

}
